use std::sync::{
    atomic::{AtomicBool, Ordering},
    mpsc, Arc,
};

use std::{io, thread, time::Duration};

use termion::{
    event::Key,
    input::{MouseTerminal, TermRead},
    raw::IntoRawMode,
    screen::AlternateScreen,
};

use tui::{
    backend::TermionBackend,
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Style},
    text::{Span, Spans, Text},
    widgets::{Block, Borders, Clear, List, ListItem, Paragraph},
    Terminal,
};

use unicode_width::UnicodeWidthStr;

pub enum Event<I> {
    Input(I),
    Tick,
}

#[allow(dead_code)]
#[derive(PartialEq, Clone, Debug)]
enum Mode {
    Insert,
    Normal,
    Message,
}

#[allow(dead_code)]
#[derive(PartialEq, Clone, Debug)]
struct StackWithHistory {
    current: usize,
    history: Vec<Vec<f64>>,
}

impl Default for StackWithHistory {
    fn default() -> Self {
        Self {
            current: 0,
            history: vec![Vec::new()],
        }
    }
}

impl StackWithHistory {
    fn push(&mut self, value: f64) {
        let prev_stack = self.history.get_mut(self.current).unwrap();
        prev_stack.push(value);
    }

    // Save state of the stack currently to the history
    fn commit(&mut self) {
        let prev_stack = self.history.get(self.current).unwrap().clone();
        if self.current < self.history.len() - 1 {
            self.history.drain(self.current + 1..);
        }
        self.current += 1;
        self.history.push(prev_stack);
    }

    fn clear(&mut self) {
        if self.current < self.history.len() - 1 {
            self.history.drain(self.current + 1..);
        }
        self.current += 1;
        self.history.push(Vec::new());
    }

    fn pop(&mut self) -> Option<f64> {
        self.history.get_mut(self.current).unwrap().pop()
    }

    fn drop(&mut self) {
        self.pop();
    }

    fn current(&self) -> Option<&Vec<f64>> {
        assert!(self.current < self.history.len());
        self.history.get(self.current)
    }

    fn undo(&mut self) -> Result<(), String> {
        if self.current > 0 {
            self.current -= 1;
            return Ok(());
        }
        Err(String::from("Nothing to undo"))
    }

    fn redo(&mut self) -> Result<(), String> {
        if self.current < self.history.len() - 1 {
            self.current += 1;
            return Ok(());
        }
        Err(String::from("Nothing to redo"))
    }
}

#[cfg(test)]
mod stack_test {
    use super::StackWithHistory;

    #[test]
    fn clearing() {
        // Clearing creates a new empty entry in the history
        let mut stack: StackWithHistory = Default::default();
        stack.push(30.0);
        assert_eq!(stack.history, vec![vec![30.0]]);
        stack.clear();
        assert_eq!(stack.history, vec![vec![30.0], vec![]]);
    }

    #[test]
    fn pushing() {
        // pushing to the stack should push to the current history index
        let mut stack: StackWithHistory = Default::default();
        stack.push(30.0);
        assert_eq!(stack.history, vec![vec![30.0]]);
        assert_eq!(stack.current, 0);
        stack.push(23.0);
        assert_eq!(stack.history, vec![vec![30.0, 23.0]]);
    }

    #[test]
    fn commiting() {
        let mut stack: StackWithHistory = Default::default();
        stack.push(30.0);
        stack.push(23.0);
        stack.commit();
        assert_eq!(stack.current, 1);
        assert_eq!(stack.current(), Some(&vec![30.0, 23.0]));
        assert_eq!(stack.history, vec![vec![30.0, 23.0], vec![30.0, 23.0]])
    }

    #[test]
    fn getting() {
        let mut stack: StackWithHistory = Default::default();
        stack.push(30.0);
        assert_eq!(stack.current(), Some(&vec![30.0f64]));
    }

    #[test]
    fn undo() {
        let mut stack: StackWithHistory = Default::default();
        stack.push(30.0);
        stack.commit();
        stack.push(23.0);
        assert_eq!(stack.current, 1);
        assert_eq!(stack.current(), Some(&vec![30.0, 23.0]));
        assert_eq!(stack.history, vec![vec![30.0], vec![30.0, 23.0]]);
        stack.undo().unwrap();
        assert_eq!(stack.current(), Some(&vec![30.0]));
        assert_eq!(stack.history, vec![vec![30.0], vec![30.0, 23.0]]);
    }

    #[test]
    #[should_panic]
    fn undo_passed_the_beginning() {
        let mut stack: StackWithHistory = Default::default();
        stack.push(30.0);
        stack.push(23.0);
        assert_eq!(stack.current(), Some(&vec![30.0, 23.0]));
        stack.undo().unwrap();
        stack.undo().unwrap();
        stack.undo().unwrap();
        stack.undo().unwrap();
        assert_eq!(stack.current(), Some(&vec![30.0, 23.0]));
    }

    #[test]
    fn pushing_after_undoing() {
        let mut stack: StackWithHistory = Default::default();
        stack.push(30.0);
        stack.commit();
        stack.push(23.0);
        stack.commit();
        stack.push(17.0);
        stack.undo().unwrap();
        stack.undo().unwrap();
        stack.commit();
        stack.push(43.0);
        assert_eq!(stack.current, 1);
        assert_eq!(stack.history, vec![vec![30.0], vec![30.0, 43.0]]);
        assert_eq!(stack.current(), Some(&vec![30.0, 43.0]));
    }

    #[test]
    #[should_panic]
    fn redo() {
        let mut stack: StackWithHistory = Default::default();
        stack.push(30.0);
        stack.push(23.0);
        assert_eq!(stack.current(), Some(&vec![30.0, 23.0]));
        stack.undo().unwrap();
        stack.redo().unwrap();
        assert_eq!(stack.current(), Some(&vec![30.0, 23.0]));
    }

    #[test]
    fn popping() {
        let mut stack: StackWithHistory = Default::default();
        stack.push(30.0);
        stack.push(23.0);
        assert_eq!(stack.pop(), Some(23.0));
        assert_eq!(stack.history, vec![vec![30.0]]);
    }

    #[test]
    fn undoing_popping_pushing() {
        let mut stack: StackWithHistory = Default::default();
        stack.push(30.0);
        stack.push(23.0);
        stack.commit();

        let first = stack.pop().unwrap();
        let second = stack.pop().unwrap();
        stack.push(first + second);
        assert_eq!(stack.history, vec![vec![30.0, 23.0], vec![53.0],])
    }
}

// App holds the state of the application
struct AppInner {
    // Current value of the input box
    input: String,
    // Current input mode
    input_mode: Mode,
    // Stack with a linear history of the changes
    stack: StackWithHistory,
    // A history of all the actions that where made numbers pushed to the
    // stack and operations
    history: Vec<String>,
    message: String,
}

// I need RefCell because the borrowing rules to be checked at run time to be able
// to check the
struct App(RefCell<AppInner>);

impl App {
    fn default() -> Self {
        Self(RefCell::new(AppInner {
            input_mode: Mode::Insert,
            input: String::with_capacity(10),
            history: Vec::with_capacity(10),
            stack: Default::default(),
            message: String::with_capacity(20),
        }))
    }

    /// Operation it's a closure with the operation that would be made in the top of the stack
    /// c is the character that represents that operation '+' for addition
    /// Example for applying a subtration
    /// ```
    /// app.apply_binary_operation(|x, y| x - y, '-')
    /// ```
    fn apply_binary_operation<F>(&self, operation: F, c: &str) -> Result<f64, &str>
    where
        F: Fn(f64, &f64) -> f64,
    {
        // stay at the same level of abstraction
        let mut app_inner = self.0.borrow_mut();

        if !app_inner.input.is_empty() {
            let input = app_inner.input.drain(..).collect::<String>();
            app_inner.stack.commit();
            app_inner.stack.push(input.parse().unwrap());
            app_inner.history.push(input.parse().unwrap());
        }

        if app_inner.stack.history[app_inner.stack.current].len() < 2 {
            return Err("Not enough elements in the stack");
        }
        app_inner.stack.commit();
        let second = app_inner.stack.pop().ok_or("Stack empty")?;
        let first = app_inner.stack.pop().ok_or("Stack empty")?;
        let result = operation(first, &second);
        app_inner.history.push(format!("{} {}", c, result));
        app_inner.stack.push(result);
        Ok(result)
    }

    // fn push_to_history(&self, s: &str) {
    //     let mut app_inner = self.0.borrow_mut();
    //     app_inner.history.push(s.to_string());
    // }

    fn commit_to_stack_history(&self) {
        let mut app_inner = self.0.borrow_mut();
        app_inner.stack.commit();
    }

    // TODO: find a way to not have to clone everytime
    fn get_input_mode(&self) -> Mode {
        let app_inner = self.0.borrow();
        app_inner.input_mode.clone()
    }

    fn set_input_mode(&self, mode: Mode) {
        let mut app_inner = self.0.borrow_mut();
        app_inner.input_mode = mode;
    }

    fn undo(&self) -> Result<(), String> {
        let mut app_inner = self.0.borrow_mut();
        // write the current state of the stack to the history
        let result = app_inner.stack.undo();
        app_inner.history = app_inner
            .stack
            .current()
            .unwrap()
            .iter()
            .to_owned()
            .map(|val| val.to_string())
            .collect::<Vec<String>>();
        result
    }

    fn redo(&self) -> Result<(), String> {
        let mut app_inner = self.0.borrow_mut();
        // write the current state of the stack to the history
        let result = app_inner.stack.redo();
        app_inner.history = app_inner
            .stack
            .current()
            .unwrap()
            .iter()
            .to_owned()
            .map(|val| val.to_string())
            .collect::<Vec<String>>();
        result
    }

    fn get_input(&self) -> String {
        let app_inner = self.0.borrow();
        app_inner.input.clone()
    }

    fn get_history(&self) -> Vec<String> {
        let app_inner = self.0.borrow();
        app_inner.history.clone()
    }

    fn get_message(&self) -> String {
        let app_inner = self.0.borrow();
        app_inner.message.clone()
    }

    fn get_stack(&self) -> Vec<f64> {
        let app_inner = self.0.borrow();
        app_inner.stack.current().unwrap().clone()
    }

    fn clear_stack(&self) {
        let mut app_inner = self.0.borrow_mut();
        app_inner.stack.clear();
        app_inner.history.drain(..);
    }

    fn drain_input(&self) -> String {
        let mut app_inner = self.0.borrow_mut();
        app_inner.input.drain(..).collect::<String>()
    }

    fn push_to_stack(&self, value: f64) {
        let mut app_inner = self.0.borrow_mut();
        app_inner.stack.push(value);
        app_inner.history.push(value.to_string());
    }

    fn pop_stack(&self) -> Option<f64> {
        let mut app_inner = self.0.borrow_mut();
        app_inner.stack.pop()
    }

    fn swap_stack(&self) -> Result<(), &str> {
        if self.get_stack().len() < 2 {
            return Err("Not enough elements in the stack");
        }
        self.commit_to_stack_history();
        let top = self.pop_stack().unwrap();
        let second_top = self.pop_stack().unwrap();

        self.push_to_stack(top);
        self.push_to_stack(second_top);
        Ok(())
    }

    fn drop_stack(&self) {
        self.commit_to_stack_history();
        let mut app_inner = self.0.borrow_mut();
        app_inner.stack.drop();
    }

    fn set_message(&self, message: &str) {
        let mut app_inner = self.0.borrow_mut();
        app_inner.message = String::from(message);
    }

    fn set_input(&self, input: String) {
        let mut app_inner = self.0.borrow_mut();
        app_inner.input = input;
    }

    fn push_input(&self, c: char) {
        let mut app_inner = self.0.borrow_mut();
        app_inner.input.push(c);
    }

    fn pop_input(&self) -> Option<char> {
        let mut app_inner = self.0.borrow_mut();
        app_inner.input.pop()
    }
}

use std::cell::RefCell;
use std::result::Result;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn apply_operation_to_empty_stack() {
        let app = App::default();
        assert!(app.apply_binary_operation(|x, y| x + y, "+").is_err());
    }

    #[test]
    fn history_after_pushing() {
        let app = App::default();
        app.commit_to_stack_history();
        app.push_to_stack(10.0);
        app.commit_to_stack_history();
        app.push_to_stack(20.0);
        assert_eq!(
            app.0.borrow().stack.history,
            vec![vec![], vec![10.0], vec![10.0, 20.0]]
        );
    }

    #[test]
    fn history_after_operation() {
        let app = App::default();
        app.commit_to_stack_history();
        app.push_to_stack(10.0);
        app.commit_to_stack_history();
        app.push_to_stack(20.0);
        assert!(!app.apply_binary_operation(|x, y| x + y, "+").is_err());
        assert_eq!(
            app.0.borrow().stack.history,
            vec![vec![], vec![10.0], vec![10.0, 20.0], vec![30.0]]
        );
    }

    #[test]
    fn check_input_mode() {
        let app = App::default();
        assert_eq!(app.get_input_mode(), Mode::Insert);
    }

    #[test]
    fn parsing() {
        let number: f64 = "1.0".parse().unwrap();
        assert_eq!(number, 1.0);
        let number: f64 = "1".parse().unwrap();
        assert_eq!(number, 1.0);
        let neg_number: f64 = "-1".parse().unwrap();
        assert_eq!(neg_number, -1.0);
    }

    #[test]
    fn pop_empty_vec() {
        let mut stack: Vec<f64> = Vec::new();
        println!("{:?}", stack.pop());
    }

    #[test]
    fn negative_zero() {
        let number = 0.0;
        let string_num = (-number).to_string();
        assert_eq!(string_num, "-0");
    }

    #[test]
    fn pop_vec() {
        let mut stack = vec![1, 2, 3];
        assert_eq!(stack.pop(), Some(3));
        assert_eq!(stack, &[1, 2])
    }
}

#[allow(dead_code)]
struct Events {
    rx: mpsc::Receiver<Event<Key>>,
    input_handle: thread::JoinHandle<()>,
    ignore_exit_key: Arc<AtomicBool>,
    tick_handle: thread::JoinHandle<()>,
}

#[derive(Debug, Clone, Copy)]
pub struct Config {
    pub exit_key: Key,
    pub tick_rate: Duration,
}

impl Default for Config {
    fn default() -> Config {
        Config {
            exit_key: Key::Char('q'),
            tick_rate: Duration::from_millis(250),
        }
    }
}

impl Events {
    pub fn new() -> Events {
        Events::with_config(Config::default())
    }

    pub fn with_config(config: Config) -> Events {
        let (tx, rx) = mpsc::channel();
        // Start in Insert mode with exit key desactivated
        let ignore_exit_key = Arc::new(AtomicBool::new(true));
        let input_handle = {
            let tx = tx.clone();
            let ignore_exit_key = ignore_exit_key.clone();
            thread::spawn(move || {
                let stdin = io::stdin();
                for key in stdin.keys().flatten() {
                    if let Err(err) = tx.send(Event::Input(key)) {
                        eprintln!("{}", err);
                        return;
                    }
                    if !ignore_exit_key.load(Ordering::Relaxed) && key == config.exit_key {
                        return;
                    }
                }
            })
        };
        let tick_handle = {
            thread::spawn(move || loop {
                if tx.send(Event::Tick).is_err() {
                    break;
                }
                thread::sleep(config.tick_rate);
            })
        };
        Events {
            rx,
            ignore_exit_key,
            input_handle,
            tick_handle,
        }
    }

    pub fn next(&self) -> Result<Event<Key>, mpsc::RecvError> {
        self.rx.recv()
    }

    pub fn disable_exit_key(&mut self) {
        self.ignore_exit_key.store(true, Ordering::Relaxed);
    }

    pub fn enable_exit_key(&mut self) {
        self.ignore_exit_key.store(false, Ordering::Relaxed);
    }
}

use std::error::Error;
const HELP: [&str; 23] = [
    "INSERT MODE",
    " Del : Delete",
    "  +  : Add",
    "  -  : Subtract",
    "  *  : Multiply",
    "  /  : Divide",
    "  ^  : Power",
    "  _  : Negate Input",
    "  n  : Negate Stack Top",
    "  d  : Drop Stack Top",
    "  s  : Swap first two",
    "  c  : Clear the stack",
    "  u  : Undo last change",
    "  U  : Redo last change",
    "  n  : Negate Stack Top",
    "<esc>: Normal Mode",
    " ",
    "NORMAL MODE",
    "  i  : Enter Insert Mode",
    "  h  : Left",
    "  j  : Down",
    "  k  : Up",
    "  l  : Right",
];

fn main() -> Result<(), Box<dyn Error>> {
    let stdout = io::stdout().into_raw_mode()?;
    let stdout = MouseTerminal::from(stdout);
    let stdout = AlternateScreen::from(stdout);
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    let app = App::default();

    let mut events = Events::new();

    terminal.clear()?;

    loop {
        terminal.draw(|f| {
            // There is a main division with an input box on the top only 3 characters
            // tall
            let main_division = Layout::default()
                .direction(Direction::Vertical)
                .constraints([Constraint::Length(3), Constraint::Min(50)].as_ref())
                .split(f.size());

            let top_division = Layout::default()
                .direction(Direction::Horizontal)
                .constraints([Constraint::Min(50), Constraint::Length(9)].as_ref())
                .split(main_division[0]);

            // The rest of the window it's divided into three vertical divisions approximately equal
            let subdivision = Layout::default()
                .direction(Direction::Horizontal)
                .constraints(
                    [
                        Constraint::Percentage(34),
                        Constraint::Percentage(33),
                        Constraint::Percentage(33),
                    ]
                    .as_ref(),
                )
                .split(main_division[1]);

            // The layout looks like this:
            // ___________________________________
            // |___________Input box________|Mode|
            // |            |          |         |
            // | Calculator | History  |  Help   |
            // | Stack      |          |  Menu   |
            // |            |          |         |
            // |---------------------------------|

            let mode_indicator = Paragraph::new(Span::styled(
                match app.get_input_mode() {
                    Mode::Normal => "Normal ",
                    Mode::Insert => "Insert ",
                    Mode::Message => "Message",
                },
                match app.get_input_mode() {
                    Mode::Normal => Style::default().fg(Color::Black).bg(Color::Blue),
                    Mode::Message => Style::default(),
                    Mode::Insert => Style::default().fg(Color::Black).bg(Color::Green),
                },
            ))
            .block(Block::default().borders(Borders::ALL).title(" Mode "));

            f.render_widget(mode_indicator, top_division[1]);

            let input_box = Paragraph::new(app.get_input())
                .style(match app.get_input_mode() {
                    Mode::Normal => Style::default(),
                    Mode::Message => Style::default(),
                    Mode::Insert => Style::default().fg(Color::Green),
                })
                .block(Block::default().borders(Borders::ALL).title(" Input "));

            f.render_widget(input_box, top_division[0]);

            let help_menu: Vec<ListItem> = HELP
                .iter()
                .map(|&item| ListItem::new(Text::raw(item)))
                .collect();

            let help_menu =
                List::new(help_menu).block(Block::default().title(" Help ").borders(Borders::ALL));

            f.render_widget(help_menu, subdivision[2]);

            let history: Vec<ListItem> = app
                .get_history()
                .iter()
                .rev()
                .enumerate()
                .map(|(i, m)| {
                    let content = vec![Spans::from(Span::raw(format!("{}: {}", i, m)))];
                    ListItem::new(content)
                })
                .collect();

            let history =
                List::new(history).block(Block::default().borders(Borders::ALL).title(" History "));

            f.render_widget(history, subdivision[1]);

            let popup = Paragraph::new(app.get_message())
                .block(Block::default().title(" Message ").borders(Borders::ALL));

            let stack: Vec<ListItem> = app
                .get_stack()
                .iter()
                .rev()
                .enumerate()
                .map(|(i, m)| {
                    let content = vec![Spans::from(Span::raw(format!("{}: {}", i, m)))];
                    ListItem::new(content)
                })
                .collect();

            let stack =
                List::new(stack).block(Block::default().borders(Borders::ALL).title(" Stack "));

            f.render_widget(stack, subdivision[0]);

            match app.get_input_mode() {
                Mode::Normal =>
                    // Hide the cursor. `Frame` does this by default, so we don't need to do anything here
                    {}
                Mode::Insert => {
                    // Make the cursor visible and ask tui-rs to put it at the specified coordinates after rendering
                    f.set_cursor(
                        // Put cursor past the end of the input text
                        main_division[0].x + app.get_input().width() as u16 + 1,
                        // Move one line down, from the border to the input line
                        main_division[0].y + 1,
                    )
                }
                Mode::Message => {
                    let area = centered_rect(60, 20, f.size());
                    f.render_widget(Clear, area); //this clears out the background
                    f.render_widget(popup, area);
                }
            }
        })?;

        if let Event::Input(input) = events.next()? {
            match app.get_input_mode() {
                Mode::Message => {
                    app.set_input_mode(Mode::Insert);
                }
                Mode::Normal => match input {
                    Key::Char('i') => {
                        events.disable_exit_key();
                        app.set_input_mode(Mode::Insert);
                    }
                    Key::Char('q') => {
                        // TODO: Save the history in a file and load it back
                        break;
                    }
                    _ => {}
                },
                Mode::Insert => match input {
                    // TODO: add some input movements like C-f for forward one character
                    // C-b: backwards one character, C-a beggining of the line,
                    // C-e: end of the line, Alt-backspace: delete one word forward
                    // Alt-d: delete one word forward, C-d delete one character forward
                    Key::Char('\n') => {
                        let input: String = app.drain_input();
                        match input.parse() {
                            Ok(value) => {
                                app.commit_to_stack_history();
                                app.push_to_stack(value);
                            }
                            Err(e) => {
                                app.set_input_mode(Mode::Message);
                                app.set_message(&e.to_string());
                            }
                        }
                    }
                    Key::Char('^') => match app.apply_binary_operation(|x, &y| x.powf(y), "^") {
                        Ok(_) => {}
                        Err(e) => {
                            app.set_input_mode(Mode::Message);
                            app.set_message(e);
                        }
                    },
                    Key::Char('+') => match app.apply_binary_operation(|x, y| x + y, "+") {
                        Ok(_) => {}
                        Err(e) => {
                            app.set_input_mode(Mode::Message);
                            app.set_message(e);
                        }
                    },
                    Key::Char('-') => match app.apply_binary_operation(|x, y| x - y, "-") {
                        Ok(_) => {}
                        Err(e) => {
                            app.set_input_mode(Mode::Message);
                            app.set_message(e);
                        }
                    },
                    Key::Char('*') => match app.apply_binary_operation(|x, y| x * y, "*") {
                        Ok(_) => {}
                        Err(e) => {
                            app.set_input_mode(Mode::Message);
                            app.set_message(e);
                        }
                    },
                    Key::Char('_') => {
                        let input_content = app.get_input();
                        if !input_content.is_empty() {
                            app.set_input(format!(
                                "{}",
                                -app.get_input().parse::<f64>().unwrap_or_default()
                            ));
                        } else {
                            app.set_input("-".to_string());
                        }
                    }
                    Key::Char('/') => match app.apply_binary_operation(|x, y| x / y, "/") {
                        Ok(_) => {}
                        Err(e) => {
                            app.set_input_mode(Mode::Message);
                            app.set_message(e);
                        }
                    },
                    Key::Char('n') => {
                        // TODO: handle printing for -0.0, I don't like -0.0
                        let input = app.get_input();
                        let stack = app.get_stack();
                        if !input.is_empty() {
                            app.set_input(format!("{}", -input.parse::<f64>().unwrap()))
                        } else if !stack.is_empty() {
                            let top = app.pop_stack().unwrap();
                            app.push_to_stack(-top);
                        }
                    }
                    Key::Char('d') => {
                        // drop the first element of the stack
                        app.drop_stack();
                    }
                    Key::Char('c') => {
                        app.clear_stack();
                    }
                    Key::Char('s') => {
                        // swap the last two positions of the stack
                        match app.swap_stack() {
                            Ok(_) => {}
                            Err(e) => {
                                app.set_input_mode(Mode::Message);
                                app.set_message(e);
                            }
                        }
                    }
                    Key::Char('u') => {
                        if let Err(err_message) = app.undo() {
                            app.set_input_mode(Mode::Message);
                            app.set_message(&err_message);
                        }
                    }
                    Key::Char('U') => {
                        if let Err(err_message) = app.redo() {
                            app.set_input_mode(Mode::Message);
                            app.set_message(&err_message);
                        }
                    }
                    Key::Char(c) => {
                        if c.is_numeric() || c == '.' {
                            app.push_input(c);
                        } else {
                            app.set_input_mode(Mode::Message);
                            app.set_message("Command not found");
                        }
                    }
                    Key::Backspace => {
                        app.pop_input();
                    }
                    Key::Esc => {
                        events.enable_exit_key();
                        app.set_input_mode(Mode::Normal);
                    }
                    _ => {}
                },
            }
        }
    }
    Ok(())
}

fn centered_rect(percent_x: u16, percent_y: u16, r: Rect) -> Rect {
    let popup_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Percentage((100 - percent_y) / 2),
                Constraint::Percentage(percent_y),
                Constraint::Percentage((100 - percent_y) / 2),
            ]
            .as_ref(),
        )
        .split(r);

    Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Percentage((100 - percent_x) / 2),
                Constraint::Percentage(percent_x),
                Constraint::Percentage((100 - percent_x) / 2),
            ]
            .as_ref(),
        )
        .split(popup_layout[1])[1]
}
