# A Reverse Polish Notation Calculator for the terminal

![rpn_calc running](example(1).png)
![rpn_calc with some input](example(2).png)

## Features

- [✅] Receive input from the user and present it in the input box.
- [✅] Validate the input and put it in the stack only if it can be parsed into f64.
- [✅] Push numbers to the stack.
- [✅] Pop and drop elements from the stack.
- [✅] Clear the stack.
- [✅] Press <kbd>q</kbd> to quit.
- [ ] Press <kbd>y</kbd> to copy the top of the stack.
- [✅] Press <kbd>u</kbd> to undo the last operation.
- [✅] Press <kbd>U</kbd> to redo the last operation.
- [ ] Adjust floating point precision

## Supported Operations

| Operation      | Integer      | Floating-Point |
| :------------: | :----------: | :------------: |
| Addition       | <kbd>+</kbd> | <kbd>+</kbd>   |
| Subtraction    | <kbd>-</kbd> | <kbd>-</kbd>   |
| Multiplication | <kbd>*</kbd> | <kbd>*</kbd>   |
| Division       | <kbd>/</kbd> | <kbd>\\</kbd>  |
| TODO: Modulus  | <kbd>%</kbd> |                |

